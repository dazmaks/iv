# iv
CLI imageboard viewer implemented in Rust.

# TODO
- [ ] Add localization
- [ ] Add image viewer
- [ ] Add ui
- [ ] Add other imageboards
- [x] Add 4chan

# References
- [2ch API](https://2ch.hk/abu/res/42375.html)
- [4chan API](https://github.com/4chan/4chan-API)

# License
[MIT](LICENSE)
